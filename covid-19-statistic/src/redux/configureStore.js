import { applyMiddleware, compose, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import rootReducer from './../Reducers';

// tich hop devtool
const composeEnhances =
  process.env.NODE_ENV !== 'production' &&
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSTION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSTION_COMPOSE__({
        shouldHotReload: false,
      })
    : compose;
const configureStore = () => {
  const middlewares = [thunk];
  const enhancers = [applyMiddleware(...middlewares)];
  const store = createStore(
    rootReducer,
    composeWithDevTools(composeEnhances(...enhancers))
  );
  return store;
};

export default configureStore;
