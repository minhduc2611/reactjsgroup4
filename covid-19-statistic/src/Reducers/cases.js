import { countryLatLong } from '../commons/Utilities/countries';
import { countryCases } from '../commons/Utilities/countryCase';
import * as casesConstants from './../commons/constants/cases';
const initialState = {
  cases: [],
  positions: [],
  countryCases: countryCases,
  countryLatLong: countryLatLong,
};

const casesReducer = (state = initialState, action) => {
  switch (action.type) {
    case casesConstants.FETCH_CASES_BY_COUNTRY:
      return { ...state, cases: [] };
    case casesConstants.FETCH_CASES_BY_COUNTRY_SUCCESS:
      const { data } = action.payload;
      let { positions } = state;
      let positionsAppend = data.map((e) => {
        // console.log(e);

        return {
          lat: e.Lat,
          lng: e.Lon,
          weight: e.Confirmed,
          province: e.Province,
          country: e.Country,
          confirmed: e.Confirmed,
          deaths: e.Deaths,
          recovered: e.Recovered,
        };
      });
      //   console.log('reducer : position cases 2', positions);
      return {
        ...state,
        cases: data,
        positions: [...positions, ...positionsAppend],
      };
    case casesConstants.FETCH_CASES_BY_COUNTRY_FAIL:
      return { ...state, cases: [] };
    default:
      return state;
  }
};

export default casesReducer;
