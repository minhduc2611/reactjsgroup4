import * as statsConstants from './../commons/constants/sumaries';
const initialState = { stats: {} };

const statsReducer = (state = initialState, action) => {
  switch (action.type) {
    case statsConstants.FETCH_STATS:
      return { ...state, stats: {} };
    case statsConstants.FETCH_STATS_SUCCESS:
      const { data } = action.payload;
      return { ...state, stats: data };
    case statsConstants.FETCH_STATS_FAIL:
      return { ...state, stats: {} };
    default:
      return state;
  }
};

export default statsReducer;
