import * as sumariesConstants from './../commons/constants/sumaries';
const initialState = { sumaries: {} };

const sumariesReducer = (state = initialState, action) => {
  switch (action.type) {
    case sumariesConstants.FETCH_SUMARIES:
      return { ...state, sumaries: {} };
    case sumariesConstants.FETCH_SUMARIES_SUCCESS:
      const { data } = action.payload;
      return { ...state, sumaries: data };
    case sumariesConstants.FETCH_SUMARIES_FAIL:
      return { ...state, sumaries: {} };
    default:
      return state;
  }
};

export default sumariesReducer;
