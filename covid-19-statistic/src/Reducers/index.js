import { combineReducers } from 'redux';
import casesReducer from './cases';
import contriesReducer from './countries';
import statsReducer from './stats';
import sumariesReducer from './sumaries';
import timeseriesReducer from './timeseries';

const rootReducer = combineReducers({
  countries: contriesReducer,
  cases: casesReducer,
  sumaries: sumariesReducer,
  series: timeseriesReducer,
  stats: statsReducer,
});

export default rootReducer;
