import * as seriesConstants from './../commons/constants/timeseries';
const initialState = { series: [] };

const timeseriesReducer = (state = initialState, action) => {
  switch (action.type) {
    case seriesConstants.FETCH_SERIES_BY_COUNTRY:
      return { ...state, series: [] };
    case seriesConstants.FETCH_SERIES_BY_COUNTRY_SUCCESS:
      const { data } = action.payload;
      //   let { series } = state;
      let seriesAppend = data.map((e) => {
        // console.log(e);

        return {
          date: e.Date,
          confirmed: e.Confirmed,
          deaths: e.Deaths,
          recovered: e.Recovered,
          province: e.Province,
        };
      });
      //   console.log('reducer : position cases 2', positions);
      return {
        ...state,
        // series: data,

        series: [...seriesAppend],
      };
    case seriesConstants.FETCH_SERIES_BY_COUNTRY_FAIL:
      return { ...state, series: [] };
    default:
      return state;
  }
};

export default timeseriesReducer;
