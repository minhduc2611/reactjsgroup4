import * as countriesConstants from './../commons/constants/countries';
const initialState = { countries: [] };

const contriesReducer = (state = initialState, action) => {
  switch (action.type) {
    case countriesConstants.FETCH_COUNTRIES:
      return { ...state, countries: [] };
    case countriesConstants.FETCH_COUNTRIES_SUCCESS:
      const { data } = action.payload;
      return { ...state, countries: data };
    case countriesConstants.FETCH_COUNTRIES_FAIL:
      return { ...state, countries: [] };
    default:
      return state;
  }
};

export default contriesReducer;
