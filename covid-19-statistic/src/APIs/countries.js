import axiosService from './../commons/axiosService';
import { API_ENDPOINT } from './../commons/constants/index';

const url = '/countries';

export const fetchCountries = () => {
  return axiosService.get(`${API_ENDPOINT}${url}`);
};
