import axios from 'axios';
import { AUTH_API } from './../commons/constants/index';

export const login = ({ email, password }) => {
  return axios.post(`${AUTH_API}/auth/login`, { email, password });
};
