import axiosService from './../commons/axiosService';
import { API_ENDPOINT } from './../commons/constants/index';

const url = '/summary';

export const fetchSumary = () => {
  return axiosService.get(`${API_ENDPOINT}${url}`);
};
const url2 = '/stats';
// stats
export const fetchStats = () => {
  return axiosService.get(`${API_ENDPOINT}${url2}`);
};
