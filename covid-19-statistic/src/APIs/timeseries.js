import moment from 'moment';
import axiosService from './../commons/axiosService';
import { API_ENDPOINT } from './../commons/constants/index';
const url = (country, from, to) => `/country/${country}?from=${from}&to=${to}`;
// 2020-03-01T00:00:00Z
// 2020-04-01T00:00:00Z
// 'https://api.covid19api.com/country/vietnam?from=2020-03-01T00:00:00Z&to=2020-04-01T00:00:00Z'
var config = {
  headers: { 'Access-Control-Allow-Origin': '*' },
};

var to =
  moment(new Date() - 60 * 60 * 24 * 1000 * 1).format('YYYY-MM-DDTHH:mm:ss') +
  'Z'; //now
var from =
  moment(new Date() - 60 * 60 * 24 * 1000 * 7).format('YYYY-MM-DDTHH:mm:ss') +
  'Z';

export const fetchSeriesByCountry = (country) => {
  return axiosService.get(`${API_ENDPOINT}${url(country, from, to)}`, config);
};
