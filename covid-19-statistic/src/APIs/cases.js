import axiosService from './../commons/axiosService';
import { API_ENDPOINT } from './../commons/constants/index';

const url = (country) => `/live/country/${country}/status/confirmed`;
var config = {
  headers: { 'Access-Control-Allow-Origin': '*' },
};
export const fetchCasesByCountry = (country) => {
  return axiosService.get(`${API_ENDPOINT}${url(country)}`, config);
};
