const style = (theme) => {
  return {
    text: {
      color: theme.dark.text_primary_color.color,
    },
    bgBlack: {
      backgroundColor: theme.dark.dark_primary_color.background,
    },
    sidebarContainer: {
      paddingRight: theme.spacing(1),
      paddingLeft: theme.spacing(2),
      [theme.breakpoints.down('sm')]: {
        paddingRight: theme.spacing(2),
        marginBottom: theme.spacing(2)
      },
    },
    contentContainer: {
      paddingRight: theme.spacing(2),
      paddingLeft: theme.spacing(1),
      [theme.breakpoints.down('sm')]: {
        paddingLeft: theme.spacing(2),
      },
    },
    App: {
      backgroundColor: 'black',
      height: '100vh',
      color: 'white',
      [theme.breakpoints.down('sm')]: {
        height: '100%',
      },
    },
  };
};
export default style;
