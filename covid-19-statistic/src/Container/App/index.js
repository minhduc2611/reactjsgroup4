import { Grid, withStyles } from '@material-ui/core';
import { PropTypes } from 'prop-types';
import React, { Component } from 'react';
import {
  NotificationContainer,
  NotificationManager,
} from 'react-notifications';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ContentContainer from '../../Components/ContentContainer';
import HeaderContainer from '../../Components/HeaderContainer';
import SidebarContainer from '../../Components/SidebarContainer';
import * as countriesActions from './../../Actions/countries';
import './App.css';
import style from './style';

class App extends Component {
  constructor(props) {
    super(props);

    if (!localStorage.getItem('token')) {
      this.props.history.push('/login');
    }
    NotificationManager.warning('Wait a second, heavy data is loading ...');
  }
  render() {
    var { classes } = this.props;
    return (
      <div className={classes.App}>
        <Grid container>
          <Grid item xs={12} sm={12} md={12}>
            <HeaderContainer />
          </Grid>
          <Grid
            className={classes.sidebarContainer}
            xs={12}
            // sm={12}
            item
            md={3}
          >
            <SidebarContainer />
          </Grid>
          <Grid
            className={classes.contentContainer}
            item
            xs={12}
            sm={12}
            md={9}
          >
            <ContentContainer />
          </Grid>
        </Grid>
        <NotificationContainer />
      </div>
    );
  }
}

App.propTypes = {
  countriesActionsCreator: PropTypes.shape({
    getCountriesRequest: PropTypes.func,
  }),
};
const mapStateToProps = null;
const mapDispatchToProps = (dispatch) => {
  return {
    countriesActionsCreator: bindActionCreators(countriesActions, dispatch),
  };
};
export default withStyles(style)(
  connect(mapStateToProps, mapDispatchToProps)(App)
);
