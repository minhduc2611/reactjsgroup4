import { withStyles, Paper, Grid, TextField, FormControlLabel, Checkbox, Button } from "@material-ui/core";
import React, { Component } from 'react';
import ContentStyle from './style';
import * as AuthService from './../../APIs/auth';
import {NotificationContainer, NotificationManager} from 'react-notifications';

class ContentContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
    };

    localStorage.clear();
  }

  handleChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  handleSubmit = async () => {
    try {
      const { email, password } = this.state;
      const token = await AuthService.login({email, password});

      localStorage.setItem("token", JSON.stringify(token));
      this.props.history.push('/');
    } catch (err) {
      NotificationManager.error('Email or password is incorrect.');
    }
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.loginPage}>
        <div className={classes.logo}>
          <img className={classes.img} src="https://i.imgur.com/Tv8h80l.png" alt="logo"/>
        </div>
        <Paper className={classes.loginForm}>
          <div className={classes.margin}>
            <Grid container spacing={8} alignItems="flex-end">
              <Grid item md={true} sm={true} xs={true}>
                <TextField
                  label="Username"
                  type="email"
                  name="email"
                  onChange={this.handleChange}
                  fullWidth autoFocus required
                />
              </Grid>
            </Grid>
            <Grid container spacing={8} alignItems="flex-end">
              <Grid item md={true} sm={true} xs={true}>
                <TextField
                  name="password"
                  label="Password"
                  type="password"
                  onChange={this.handleChange}
                  fullWidth required
                />
              </Grid>
            </Grid>
            <Grid container spacing={8} alignItems="center" justify="space-between">
              <Grid item>
                <FormControlLabel control={
                  <Checkbox
                    color="primary"
                  />
                } label="Remember me" />
              </Grid>
              <Grid item>
                <Button
                  disableFocusRipple disableRipple
                  style={{ textTransform: "none" }}
                  variant="text" color="primary">Forgot password ?</Button>
              </Grid>
            </Grid>
            <Grid container justify="center" style={{ marginTop: '10px' }}>
              <Button
                type="submit"
                variant="outlined" color="primary"
                onClick={this.handleSubmit}
                style={{ textTransform: "none" }}>Login</Button>
            </Grid>
          </div>
        </Paper>
        <NotificationContainer/>
      </div>
    );
  }
}
export default withStyles(ContentStyle)(ContentContainer);
