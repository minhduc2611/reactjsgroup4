import 'react-notifications/lib/notifications.css';

const LoginStyle = (theme) => ({
    margin: {
        margin: '20px',
    },
    loginForm: {
        maxWidth: '500px',
        margin: 'auto',
        marginTop: '30px',
        paddingBottom: '10px',
        backgroundColor: '#efefeff5'
    },
    loginPage: {
        backgroundImage: 'url("https://images.latintimes.com/sites/latintimes.com/files/2020/03/11/electron-microscopy-coronavirus.jpg")',
        paddingTop: '100px',
        height: '100vh',
    },
    logo: {
        maxWidth: '500px',
        margin: 'auto',
    },
    img: {
        maxWidth: '500px',
        height: 'auth',
    }
});
export default LoginStyle;
