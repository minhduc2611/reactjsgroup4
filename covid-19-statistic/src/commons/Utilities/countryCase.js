export var countryCases = [
  {
    Country: 'Viet Nam',
    CountryCode: 'VN',
    Lat: '14.06',
    Lon: '108.28',
    Confirmed: 268,
    Deaths: 0,
    Recovered: 198,
    Active: 70,
    Date: '2020-04-18T00:00:00Z',
  },
  {
    Country: 'Japan',
    CountryCode: 'JP',
    Lat: '36.2',
    Lon: '138.25',
    Confirmed: 9787,
    Deaths: 190,
    Recovered: 935,
    Active: 8662,
    Date: '2020-04-18T00:00:00Z',
  },
];
