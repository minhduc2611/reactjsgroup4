export function groupBy(objectArray, property) {
  return objectArray.reduce(function (acc, obj) {
    var key = obj[property];
    if (!acc[key]) {
      acc[key] = [];
    }
    acc[key].push(obj);
    return acc;
  }, {});
}

export function format(countryName) {
  countryName = countryName.split(',')[0];

  if (countryName.split('-').length > 1) {
    countryName = countryName
      .split('-')
      .map((ctn) => {
        return ctn[0] === ctn[0].toLowerCase() ? ctn[0].toUpperCase() : ctn[0];
      })
      .join('');
  }
  if (
    countryName.length > 7 &&
    countryName[0] === countryName[0].toLowerCase()
  ) {
    countryName = countryName.substring(0, 5);
  }
  if (countryName[0] === countryName[0].toLowerCase()) {
    var strArr = countryName.split('');
    strArr[0] = strArr[0].toUpperCase();
    countryName = strArr.join('');
  }
  return countryName;
}
