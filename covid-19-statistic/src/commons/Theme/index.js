import { createMuiTheme } from '@material-ui/core/styles';
const theme = createMuiTheme({
  dark: {
    dark_primary_color: { background: '#575757' }, //Main Back ground
    text_primary_color: { color: '#c7c7c7' }, //Main Text
    divider_color: { borderColor: '#BDBDBD' }, // Main Border color

    default_primary_color: { background: '#607D8B ' },
    light_primary_color: { background: '#CFD8DC ' },
    accent_color: { background: '#9E9E9E' },
    primary_text_color: { color: '#212121' },
    secondary_text_color: { color: '#757575' },

    font: { fontFamily: 'Roboto' },
  },
});
export default theme;
