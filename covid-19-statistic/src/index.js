import { ThemeProvider } from '@material-ui/core';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import theme from './commons/Theme';
import App from './Container/App';
import Login from './Container/Login';
import './index.css';
import configureStore from './redux/configureStore';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
const store = configureStore();
ReactDOM.render(
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <BrowserRouter>
        <Switch>
          <Route path='/login' component={ Login }/>
          <Route path='/' component={ App }/>
        </Switch>
      </BrowserRouter>
    </ThemeProvider>
  </Provider>,
  document.getElementById('root')
);
serviceWorker.unregister();
