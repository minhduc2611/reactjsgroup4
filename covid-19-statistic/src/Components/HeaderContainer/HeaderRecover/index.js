import { Chart, LineSeries } from '@devexpress/dx-react-chart-material-ui';
import { Grid, withStyles } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import React, { Component } from 'react';
import HeaderStyle from '../style';

class HeaderRecover extends Component {
  render() {
    var { classes, sumary } = this.props;
    const data = [
      { argument: 1, value: 7 },
      { argument: 2, value: sumary.totalRecovered / 12 },
      { argument: 3, value: sumary.totalRecovered / 11 },
      { argument: 4, value: sumary.totalRecovered / 11 },
      { argument: 5, value: sumary.totalRecovered / 10 },
      { argument: 6, value: sumary.totalRecovered / 5 },
      { argument: 7, value: sumary.totalRecovered / 3 },
      { argument: 8, value: sumary.totalRecovered / 2 },
      { argument: 10, value: sumary.totalRecovered },
    ];
    return (
      <div className={[classes.paperDiv, classes.prSpacing2].join(' ')}>
        <Paper className={[classes.paper, classes.bgGrey].join(' ')}>
          <Grid container>
            <Grid item xs={6}>
              <Typography className={[classes.smText].join(' ')}>
                Số người hồi phục
              </Typography>
              <Typography
                className={[classes.recoverColor, classes.boldTxt].join(' ')}
              >
                +
                {sumary.newRecovered.toLocaleString(navigator.language, {
                  minimumFractionDigits: 0,
                })}
              </Typography>
              <Typography
                className={[classes.recoverColor, classes.boldTxt].join(' ')}
              >
                {sumary.totalRecovered.toLocaleString(navigator.language, {
                  minimumFractionDigits: 0,
                })}
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Chart className={classes.height100} data={data}>
                <LineSeries
                  color="rgb(164, 201, 57)"
                  valueField="value"
                  argumentField="argument"
                />
              </Chart>
            </Grid>
          </Grid>
        </Paper>
      </div>
    );
  }
}

export default withStyles(HeaderStyle)(HeaderRecover);
