// presentational component
import { Grid, withStyles } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import React, { Component } from 'react';
import HeaderStyle from '../style';

class HeaderMain extends Component {
  render() {
    var { classes, updatedDay } = this.props;
    return (
      <div className={classes.paperDiv}>
        <Paper className={[classes.paper, classes.bgGrey].join(' ')}>
          <Grid container>
            <Grid item xs={12} md={12} lg={6}>
              <Typography
                className={[classes.textCenter, classes.title].join(' ')}
              >
                COVID-19
              </Typography>
            </Grid>
            <Grid item xs={12} md={12} lg={6}>
              <Typography className={[classes.smText].join(' ')}>
                Cập nhật lần cuối lúc: {updatedDay}
              </Typography>
              <Typography className={[classes.smText].join(' ')}>
                Nguồn: WHO, CDC, NHC, DXY & Bộ Y Tế Việt Nam.
              </Typography>
            </Grid>
          </Grid>
        </Paper>
      </div>
    );
  }
}

export default withStyles(HeaderStyle)(HeaderMain);
