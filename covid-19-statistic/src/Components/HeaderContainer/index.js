//smart component
import { withStyles } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import moment from 'moment';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as sumariesActions from './../../Actions/sumaries';
import HeaderCase from './HeaderCase';
import HeaderDeath from './HeaderDeath';
import HeaderMain from './HeaderMain';
import HeaderRecover from './HeaderRecover';
import HeaderStyle from './style';
class HeaderContainer extends Component {
  render() {
    var { classes, sumaries, stats } = this.props;
    var updatedDay = stats.AllUpdated
      ? moment(
          parseInt(new Date(stats.AllUpdated).getTime()) +
            60 * 60 * 24 * 1000 * 3
        ).format('HH:mm:ss DD/MM/YYYY')
      : 'Updating';

    var sumary = {
      NewConfirmed: 0,
      TotalConfirmed: 0,
      NewDeaths: 0,
      TotalDeaths: 0,
      NewRecovered: 0,
      TotalRecovered: 0,
    };
    sumary = sumaries ? sumaries : sumary;
    const cases = {
      totalConfirm: sumary.TotalConfirmed,
      newConfirmed: sumary.NewConfirmed,
    };
    const deaths = {
      totalDeaths: sumary.TotalDeaths,
      newDeaths: sumary.NewDeaths,
    };
    const recovers = {
      totalRecovered: sumary.TotalRecovered,
      newRecovered: sumary.NewRecovered,
    };
    return (
      <div className={classes.root}>
        <Grid container>
          <Grid className={classes.headerAppName} item xs={12} lg={5}>
            <HeaderMain updatedDay={updatedDay}></HeaderMain>
          </Grid>
          <Grid item xs={6} md={4} lg={2}>
            <HeaderCase sumary={cases}></HeaderCase>
          </Grid>
          <Grid className={classes.headerDeath} item xs={6} md={4} lg={2}>
            <HeaderDeath sumary={deaths}></HeaderDeath>
          </Grid>
          <Grid item xs={12} md={4} lg={3}>
            <HeaderRecover sumary={recovers}></HeaderRecover>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    sumaries: state.sumaries.sumaries.Global,
    stats: state.stats.stats,
  };
};

//send actions by dispatch
const mapDispatchToProps = (dispatch) => {
  return {
    casesSumariesCreator: bindActionCreators(sumariesActions, dispatch),
  };
};
export default withStyles(HeaderStyle)(
  connect(mapStateToProps, mapDispatchToProps)(HeaderContainer)
);
