// presentational component
import { Chart, LineSeries } from '@devexpress/dx-react-chart-material-ui';
import { Grid, withStyles } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import React, { Component } from 'react';
import HeaderStyle from '../style';

class HeaderCase extends Component {
  render() {
    var { classes, sumary } = this.props;
    const data = [
      // since API does not have daily total values
      { argument: 1, value: 10 },
      { argument: 2, value: sumary.totalConfirm / 3 },
      { argument: 3, value: sumary.totalConfirm / 2 },
      { argument: 4, value: sumary.totalConfirm },
    ];
    return (
      <div className={classes.paperDiv}>
        <Paper className={[classes.paper, classes.bgGrey].join(' ')}>
          <Grid container>
            <Grid item xs={6}>
              <Typography className={[classes.smText].join(' ')}>
                Số người nhiễm
              </Typography>
              <Typography
                className={[classes.caseColor, classes.boldTxt].join(' ')}
              >
                +
                {sumary.newConfirmed.toLocaleString(navigator.language, {
                  minimumFractionDigits: 0,
                })}
              </Typography>
              <Typography
                className={[classes.caseColor, classes.boldTxt].join(' ')}
              >
                {sumary.totalConfirm.toLocaleString(navigator.language, {
                  minimumFractionDigits: 0,
                })}
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Chart className={classes.height100} data={data}>
                <LineSeries
                  color="rgb(189, 33, 48)"
                  valueField="value"
                  argumentField="argument"
                />
              </Chart>
            </Grid>
          </Grid>
        </Paper>
      </div>
    );
  }
}

export default withStyles(HeaderStyle)(HeaderCase);
