import { Chart, LineSeries } from '@devexpress/dx-react-chart-material-ui';
import { Grid, withStyles } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import React, { Component } from 'react';
import HeaderStyle from '../style';

class HeaderDeath extends Component {
  render() {
    var { classes, sumary } = this.props;
    const data = [
      // since API does not have daily total values
      { argument: 1, value: 2 },
      { argument: 2, value: 9 },
      { argument: 3, value: sumary.totalDeaths / 5 },
      { argument: 4, value: sumary.totalDeaths / 2 },
      { argument: 6, value: sumary.totalDeaths },
    ];
    return (
      <div className={classes.paperDiv}>
        <Paper className={[classes.paper, classes.bgGrey].join(' ')}>
          <Grid container>
            <Grid item xs={6}>
              <Typography className={[classes.smText].join(' ')}>
                Số người chết
              </Typography>
              <Typography
                className={[classes.deathColor, classes.boldTxt].join(' ')}
              >
                {' '}
                +
                {sumary.newDeaths.toLocaleString(navigator.language, {
                  minimumFractionDigits: 0,
                })}
              </Typography>
              <Typography
                className={[classes.deathColor, classes.boldTxt].join(' ')}
              >
                {sumary.totalDeaths.toLocaleString(navigator.language, {
                  minimumFractionDigits: 0,
                })}
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Chart className={classes.height100} data={data}>
                <LineSeries
                  color="white"
                  valueField="value"
                  argumentField="argument"
                />
              </Chart>
            </Grid>
          </Grid>
        </Paper>
      </div>
    );
  }
}

export default withStyles(HeaderStyle)(HeaderDeath);
