const HeaderStyle = (theme) => {
  return {
    root: {
      flexGrow: 1,
    },
    paperDiv: {
      padding: theme.spacing(1),
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(0),
      // paddingBottom: theme.spacing(0),
    },
    headerAppName: {
      [theme.breakpoints.down('sm')]: {
        paddingRight: theme.spacing(2),
        display: 'block',
      },
      [theme.breakpoints.up('md')]: {
        display: 'none',
      },
      [theme.breakpoints.up('lg')]: {
        display: 'block',
      },
    },
    headerDeath: {
      [theme.breakpoints.down('sm')]: {
        paddingRight: theme.spacing(2),
      },
    },
    paper: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(2),
      paddingTop: theme.spacing(1),
      paddingBottom: theme.spacing(1),
      verticalAlign: 'middle',
      height: '9vh',
      color: 'white',
    },
    prSpacing2: {
      paddingRight: theme.spacing(2),
    },
    bgGrey: {
      backgroundColor: theme.dark.dark_primary_color.background,
    },
    textCenter: {
      textAlign: 'center',
    },
    title: {
      fontSize: '20px',
      fontWeight: 'bold',
      [theme.breakpoints.down('md')]: {
        fontSize: '13px',
      },
    },
    smText: {
      fontSize: '12px',
    },
    caseColor: {
      color: 'rgb(189, 33, 48)',
    },
    deathColor: {
      color: 'white',
    },
    recoverColor: {
      color: 'rgb(164, 201, 57)',
    },
    boldTxt: {
      fontWeight: 'bold',
    },
    height100: {
      height: '100% !important',
    },
  };
};
export default HeaderStyle;
