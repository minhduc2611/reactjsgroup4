import {
  Badge,
  isWidthDown,
  Typography,
  withStyles,
  withWidth,
} from '@material-ui/core/';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as sumariesActions from '../../Actions/sumaries';
import SidebarMain from '../../Components/SidebarContainer/SidebarMain/index';
import SideBarStyle from './style';

class SidebarContainer extends Component {
  componentDidMount() {
    const { casesSumariesCreator } = this.props;
    const { getSumariesRequest } = casesSumariesCreator;
    getSumariesRequest();
  }

  render() {
    var { classes, sumaries, width } = this.props;

    if (sumaries) {
      var size = 212;
      if (isWidthDown('xs', width)) {
        size = 15;
      }

      sumaries.sort((a, b) => {
        if (a.TotalConfirmed < b.TotalConfirmed) return 1;
        else if (a.TotalConfirmed > b.TotalConfirmed) return -1;
        else return 0;
      });
      var listCountries = sumaries.slice(0, size).map((country, id) => {
        return {
          name: country.Country,
          // image: country.Country,
          id: country.CountryCode,
          TotalConfirmed: country.TotalConfirmed,
          TotalDeaths: country.TotalDeaths,
          TotalRecovered: country.TotalRecovered,
          // amt: 2400,
        };
      });
      var elements = listCountries.map((sumary, index) => {
        return (
          <SidebarMain
            key={index}
            name={sumary.name}
            id={sumary.id.toLowerCase()}
            confirmed={sumary.TotalConfirmed}
            recovered={sumary.TotalRecovered}
            deaths={sumary.TotalDeaths}
          ></SidebarMain>
        );
      });
      // console.log(data2);
    }

    return (
      <div className={[classes.sidebar, classes.paperDiv].join(' ')}>
        <div className={classes.sidebarHeading}>
          Số ca nhiễm theo quốc gia <br />
          <small>(212 quốc gia)</small>
        </div>
        <div className={classes.sidebarTitle}>
          <Typography variant="subtitle1">
            <Badge color="secondary" variant="dot">
              &nbsp;
            </Badge>
                        Nhiễm bệnh
          </Typography>
          <Typography variant="subtitle1">
            <Badge variant="dot" classes={{ badge: classes.customBadge1 }}>
              &nbsp;
            </Badge>
                        Bình phục
          </Typography>
          <Typography variant="subtitle1">
            <Badge variant="dot" classes={{ badge: classes.customBadge2 }}>
              &nbsp;
            </Badge>
                        Tử vong
          </Typography>
        </div>
        <div className={classes.sidebarRow}>{elements}</div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    // listCountries: state.countries.countries,
    // cases: state.cases,
    // positions: state.cases.positions,
    sumaries: state.sumaries.sumaries.Countries,
  };
};

//send actions by dispatch
const mapDispatchToProps = (dispatch) => {
  return {
    casesSumariesCreator: bindActionCreators(sumariesActions, dispatch),
  };
};
export default withWidth()(
  withStyles(SideBarStyle)(
    connect(mapStateToProps, mapDispatchToProps)(SidebarContainer)
  )
);
