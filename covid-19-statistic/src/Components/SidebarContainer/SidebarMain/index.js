// presentational component

import { withStyles } from '@material-ui/core/';
import React, { Component } from 'react';
import SideBarStyle from '../style';

class SidebarMain extends Component {
  render() {
    var { classes, name, confirmed, recovered, deaths, id } = this.props;
    return (
      <li className={classes.sidebarRowElement}>
        <div>
          <img
            alt={id}
            src={'https://www.countryflags.io/' + [id] + '/flat/64.png'}
            style={{ width: '15px', marginRight: '5px', marginLeft: '10px' }}
          />
          &nbsp; &nbsp; &nbsp;
          {name}
        </div>
        <div>
          <span
            className="badge badge-secondary badge-pill"
            style={{ backgroundColor: 'rgb(189,33,48)' }}
          >
            {confirmed.toLocaleString(navigator.language, {
              minimumFractionDigits: 0,
            })}
          </span>
          &nbsp;
          <span
            className="badge badge-secondary badge-pill"
            style={{ backgroundColor: 'rgb(164,201,57)' }}
          >
            {recovered.toLocaleString(navigator.language, {
              minimumFractionDigits: 0,
            })}
          </span>
          &nbsp;
          <span
            className="badge badge-secondary badge-pill"
            style={{ backgroundColor: 'rgb(189,189,189)' }}
          >
            {deaths.toLocaleString(navigator.language, {
              minimumFractionDigits: 0,
            })}
          </span>
        </div>
      </li>
    );
  }
}

export default withStyles(SideBarStyle)(SidebarMain);
