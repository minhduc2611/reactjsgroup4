// component style here. dont forget to use theme. demo is at the App folder: style.js

const SideBarStyle = (theme) => {
  return {
    paperDiv: {
      // marginRight: theme.spacing(2),
      // paddingLeft: theme.spacing(3),
      borderRadius: '5px',
    },
    sidebar: {
      width: '100%',
      height: '88vh',
      // height: '100%',
      backgroundColor: theme.dark.dark_primary_color.background,
      color: '#bdbdbd',
      // margin: "5px",
      border: '1px solid #363636',
      overflowY: 'scroll',
      overflowX: 'hidden',
      position: 'relative',

      // paddingLeft: "0"
    },
    sidebarHeading: {
      textAlign: 'center',
      padding: '10px',
      fontWeight: 'bold',
      fontSize: '15px',
    },
    sidebarTitle: {
      textAlign: 'left',
      fontSize: '10px',
      paddingLeft: '20px',
      margin: '10px 0px',
    },
    sidebarRow: {},
    sidebarRowElement: {
      textAlign: 'left',
      listStyleType: 'none',
      marginLeft: '0',
      border: 'none',
      padding: '10px 0px 10px 12px',
      fontSize: '12px',
      borderBottom: '1px solid #363636',
    },
    image: {
      width: '15px',
      marginRight: '5px',
      marginLeft: '10px',
    },
    redBagde: {
      backgroundColor: 'rgb(189,33,48)',
    },
    margin: {
      margin: theme.spacing(2),
    },
    customBadge1: {
      backgroundColor: '#A4C939',
      color: 'white',
    },
    customBadge2: {
      backgroundColor: '#BDBDBD',
      color: 'white',
    },
  };
};
export default SideBarStyle;
