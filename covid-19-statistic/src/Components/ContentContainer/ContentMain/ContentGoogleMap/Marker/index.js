import React from 'react';

export default function Marker(props) {
  var { sum } = props;
  let provinceStr = sum.province ? <li>Province: {sum.province} </li> : ``;
  return (
    <div style={{ cursor: 'pointer' }}>
      <span style={{ zIndex: -1 }}>o</span>
      <div
        style={
          props.$hover
            ? {
                display: 'block',
                width: '150px',
                backgroundColor: 'gray',
                zIndex: '100',
                position: 'absolute',
                padding: 5,
                // float: 'left',
              }
            : { display: 'none' }
        }
      >
        <strong>{sum.country}</strong> :{provinceStr}
        <li>Confirmed: {sum.confirmed}, </li>
        <li>Deaths: {sum.deaths}, </li>
        <li>Recovered: {sum.recovered}</li>
      </div>
    </div>
  );
}
