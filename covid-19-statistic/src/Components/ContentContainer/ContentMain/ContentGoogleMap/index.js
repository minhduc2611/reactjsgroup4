import { Button, withStyles } from '@material-ui/core';
import GoogleMapReact from 'google-map-react';
import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as countriesActions from './../../../../Actions/countries';
import { GOO_API_KEY } from './../../../../commons/constants/index';
import ContentStyle from './../../style';
import Marker from './Marker';
class ContentGoogleMap extends Component {
  componentDidMount() {
    // const { countriesActionsCreator } = this.props;
    // const { getCountriesWithCasesRequest } = countriesActionsCreator;
    // getCountriesWithCasesRequest();
  }

  constructor(props) {
    super(props);
    this.state = {
      heatmapVisible: true,
    };
  }
  static defaultProps = {
    center: {
      lat: 50.219574172499215,
      lng: 2.5102104818604776,
    },
    zoom: 4,
  };

  toggleHeatMap() {
    this.setState(
      {
        heatmapVisible: !this.state.heatmapVisible,
      },
      () => {
        if (this._googleMap !== undefined) {
          this._googleMap.heatmap.setMap(
            this.state.heatmapVisible ? this._googleMap.map_ : null
          );
        }
      }
    );
  }
  _onClick = ({ x, y, lat, lng }) => console.log(x, y, lat, lng);

  changeGradient = () => {
    var gradient = [
      'rgba(0, 255, 255, 0)',
      '#d4d9d8',
      '#4034eb',
      '#34ebd3',
      '#216658',
    ];
    this._googleMap.heatmap.set(
      'gradient',
      this._googleMap.heatmap.get('gradient') ? null : gradient
    );
  };

  render() {
    var {
      classes,
      // positions,
      toggleGradient,
      // countryCases,
      countryLatLong,
      sumaries,
    } = this.props;

    var result = [];
    _.assignIn(result, countryLatLong, sumaries);
    result = _(countryLatLong)
      .concat(sumaries)
      .groupBy('CountryCode')
      .map(_.spread(_.assign))
      .value();

    result = result.filter(
      (el) => !(el.latitude === '' || el.latitude === undefined)
    );

    let gradient = toggleGradient
      ? ['rgba(0, 255, 255, 0)', '#d4d9d8', '#4034eb', '#34ebd3', '#216658']
      : null;

    var heatmapPosition = result.map((ctc) => {
      return {
        lat: ctc.latitude,
        lng: ctc.longitude,
        weight: ctc.TotalConfirmed,
      };
    });
    var positionsMarker = result.map((ctc) => {
      return {
        lat: ctc.latitude,
        lng: ctc.longitude,
        weight: ctc.TotalConfirmed,
        // province: ctc.Province,
        country: ctc.Country,
        confirmed: ctc.TotalConfirmed,
        deaths: ctc.TotalDeaths,
        recovered: ctc.TotalRecovered,
        newConfirmed: ctc.NewConfirmed,
        newDeaths: ctc.NewDeaths,
        newRecovered: ctc.NewRecovered,
        date: ctc['Date'],
      };
    });

    const heatmapData = {
      positions: heatmapPosition,
      options: {
        radius: 50,
        opacity: 1.5,
        gradient: gradient,
      },
    };
    /*  //Example of Marker
    var renderMarkers = (map, maps) => {
      let marker = new maps.Marker({
        position: { lat: 44.27, lng: -89.62 },
        map,
        title: '22222222',
      });
      marker.setMap(map);
    };
    */
    return (
      <div className={classes.containerMap}>
        <div className={classes.btnMaps}>
          <Button
            style={{ marginLeft: '10px' }}
            variant="contained"
            onClick={() => this.toggleHeatMap()}
          >
            Show Heat
          </Button>
          <Button
            variant="contained"
            style={{ marginLeft: '10px' }}
            onClick={() => this.changeGradient()}
          >
            Change Gradient
          </Button>
        </div>

        <GoogleMapReact
          className={classes.theMap}
          style={{ height: '95%' }}
          ref={(el) => (this._googleMap = el)}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
          heatmapLibrary={true}
          heatmap={heatmapData}
          bootstrapURLKeys={GOO_API_KEY}
          // onGoogleApiLoaded={({ map, maps }) => renderMarkers(map, maps)}
          yesIWantToUseGoogleMapApiInternals={true}
          onClick={this._onClick}
        >
          {positionsMarker.map((sum, id) => {
            return <Marker key={id} lat={sum.lat} lng={sum.lng} sum={sum} />;
          })}
        </GoogleMapReact>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    // countryCases: state.cases.countryCases,
    countryLatLong: state.cases.countryLatLong,
    // positions: state.cases.positions,
    sumaries: state.sumaries.sumaries.Countries,
  };
};

//send actions by dispatch
const mapDispatchToProps = (dispatch) => {
  return {
    countriesActionsCreator: bindActionCreators(countriesActions, dispatch),
  };
};
export default withStyles(ContentStyle)(
  connect(mapStateToProps, mapDispatchToProps)(ContentGoogleMap)
);

/**
 *
 *
 * Reference : https://github.com/google-map-react/google-map-react/blob/master/API.md
 *
 *
 *
 */
