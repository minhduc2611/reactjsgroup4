import { TextField, withStyles } from '@material-ui/core';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { format } from '../../../../commons/Utilities';
import * as sumariesActions from './../../../../Actions/sumaries';
import ContentStyle from './../../style';
import ContentDescribe from './ContentDescribe';
import MyStackChart from './MyStackChart';

class ContentStackChart extends Component {
  componentDidMount() {
    const { casesSumariesCreator } = this.props;
    const { getSumariesRequest } = casesSumariesCreator;
    getSumariesRequest();
  }

  constructor(props) {
    super(props);
    this.state = {
      chosenChart: {},
      keyword: '',
    };
  }
  onHandleChoseChart = (chosen) => {
    this.setState({ chosenChart: chosen ? chosen : {} });
  };
  render() {
    var { sumaries, classes } = this.props;
    var { keyword } = this.state;

    // console.log(sumaries);
    if (sumaries !== undefined) {
      var size = 15;
      sumaries.sort((a, b) => {
        if (a.TotalConfirmed < b.TotalConfirmed) return 1;
        else if (a.TotalConfirmed > b.TotalConfirmed) return -1;
        else return 0;
      });

      if (keyword !== '') {
        sumaries = sumaries.filter((country) => {
          return country.Country.toLowerCase().indexOf(keyword) !== -1;
        });
      }

      var topCountries = sumaries.slice(0, size).map((country, id) => {
        return {
          name: format(country.Slug),
          CountryName: format(country.Country),
          slug: country.Slug,
          TotalConfirmed: country.TotalConfirmed,
          TotalDeaths: country.TotalDeaths,
          TotalRecovered: country.TotalRecovered,
        };
      });

      // console.log(data2);
    }

    var defaultData = [
      {
        name: '',
        uv: 0,
        pv: 0,
        amt: 0,
      },
    ];

    return (
      <div style={{ width: '100%', height: '100%', position: 'relative' }}>
        <div className={classes.searchContainer}>
          <TextField
            className={classes.Input}
            id="outlined-basic"
            label="Search"
            variant="outlined"
            value={keyword}
            onChange={(e) => this.setState({ keyword: e.target.value })}
          />
        </div>
        <div className={classes.chartContainer}>
          <MyStackChart
            onHandleChoseChart={this.onHandleChoseChart}
            data={topCountries ? topCountries : defaultData}
          />
          <ContentDescribe chosenChart={this.state.chosenChart} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    positions: state.cases.positions,
    sumaries: state.sumaries.sumaries.Countries,
  };
};

//send actions by dispatch
const mapDispatchToProps = (dispatch) => {
  return {
    casesSumariesCreator: bindActionCreators(sumariesActions, dispatch),
  };
};
export default withStyles(ContentStyle)(
  connect(mapStateToProps, mapDispatchToProps)(ContentStackChart)
);
