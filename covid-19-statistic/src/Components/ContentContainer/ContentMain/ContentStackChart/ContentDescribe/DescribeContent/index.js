import { withStyles } from '@material-ui/core';
import React from 'react';
import ContentStyle from './../../../../style';
function DescribeContent(props) {
  var { chosen } = props;
  var name = chosen.activePayload
    ? chosen.activePayload[0].payload.CountryName
    : chosen.activeLabel;
  // var to = moment(new Date()).format('DD-MM-YYYY'); //now
  // var from = moment(new Date() - 60 * 60 * 24 * 1000 * 14).format(
  //   'DD-MM-YYYY' //HH:mm:ss
  // );
  return (
    <div
      style={{
        textAlign: 'center',
        fontSize: '15px',
        border: '1px gray solid',
        padding: '7px',
        margin: '1%',
        position: 'absolute',
        width: '98%',
        bottom: '0',
      }}
    >
      Name : <strong style={{ fontSize: '25px' }}> {name} </strong>
      {chosen.activePayload.map((pl, id) => {
        return (
          <span key={id} style={{ color: pl.fill }}>
            {' '}
            {pl.dataKey} : {pl.value}
          </span>
        );
      })}
    </div>
  );
}
export default withStyles(ContentStyle)(DescribeContent);
