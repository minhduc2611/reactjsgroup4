import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  withStyles,
} from '@material-ui/core';
import React from 'react';
import ContentStyle from '../../../../../style';

function MySelectOption(props) {
  var { classes, chosenProvince, handleChange, groupedSeries } = props;
  return (
    <div className={classes.formControlContainerLineChart}>
      <FormControl className={classes.formControl}>
        <InputLabel id="demo-simple-select-label">Select Province:</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={chosenProvince}
          onChange={(e) => handleChange(e)}
        >
          {Object.keys(groupedSeries).map((key, id) => (
            <MenuItem key={id} value={key}>
              {key}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      {/* <h2>{chosenProvince}</h2> */}
    </div>
  );
}

export default withStyles(ContentStyle)(MySelectOption);
