import { withStyles } from '@material-ui/core';
import moment from 'moment';
import React from 'react';
import {
  ResponsiveContainer,
  Scatter,
  ScatterChart,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts';
import ContentStyle from './../../../../../style';

const CustomTooltip = (props) => {
  const { active } = props;

  if (active) {
    const { payload } = props;
    // console.log('payload', payload);

    return (
      <div style={{ color: 'black', backgroundColor: 'white', padding: 5 }}>
        <p>
          {payload[0].name} :
          {moment(parseInt(payload[0].value)).format('DD-MM-YYYY')}
        </p>
        <p>
          {payload[1].name} :{' '}
          {payload[1].value.toLocaleString(navigator.language, {
            minimumFractionDigits: 0,
          })}
        </p>
      </div>
    );
  }

  return null;
};

function MyLineChart(props) {
  var { chartData } = props;

  return (
    <ResponsiveContainer color="red" width="100%" height="110%">
      <ScatterChart>
        <XAxis
          tick={{ fill: 'white' }}
          stroke="white"
          dataKey="time"
          domain={['auto', 'auto']}
          name="Time"
          tickFormatter={(unixTime) => moment(unixTime).format('DD-MM-YYYY')}
          type="number"
        />
        <YAxis
          stroke="white"
          tick={{ fill: 'white' }}
          tickFormatter={(num) =>
            num.toLocaleString(navigator.language, {
              minimumFractionDigits: 0,
            })
          }
          dataKey="value"
          name="People"
        />
        <Tooltip content={<CustomTooltip />} />
        <Scatter
          data={chartData.chartSeriesConfirmed}
          fill={'red'}
          line={{ stroke: 'white' }}
          lineJointType="monotoneX"
          lineType="joint"
          name="Confirmed Cases"
          color={'red'}
        />
        <Scatter
          data={chartData.chartSeriesDeath}
          fill={'gray'}
          line={{ stroke: 'white' }}
          lineJointType="monotoneX"
          lineType="joint"
          name="Death Cases"
        />
        <Scatter
          data={chartData.chartSeriesRecover}
          fill={'green'}
          line={{ stroke: 'white' }}
          lineJointType="monotoneX"
          lineType="joint"
          name="Recoverd Cases"
        />
      </ScatterChart>
    </ResponsiveContainer>
  );
}
export default withStyles(ContentStyle)(MyLineChart);
