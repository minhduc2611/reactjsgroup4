import { withStyles } from '@material-ui/core';
import moment from 'moment';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as seriesActions from '../../../../../../Actions/timeseries';
import { groupBy } from '../../../../../../commons/Utilities/index';
import ContentStyle from '../../../../style';
import MyLineChart from './MyLineChart';
import MySelectOption from './MySelectOption';
// import DescribeContent from './DescribeContent';
// import DescribeLineChart from './DescribeLineChart';

class DescribeLineChart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      slug: this.props.slug,
      chosenProvince: '',
      openSelection: false,
    };
  }
  componentDidMount() {
    this.genLineChartData(this.state.slug);
  }

  genLineChartData = (slug) => {
    const { seriesActionsCreator } = this.props;
    const { getSeriesByCountryRequest } = seriesActionsCreator;
    getSeriesByCountryRequest(slug);
  };

  componentDidUpdate(prevProps) {
    if (this.props.slug !== prevProps.slug) {
      // Check if it's a new user, you can also use some unique property, like the ID  (this.props.user.id !== prevProps.user.id)
      this.genLineChartData(this.state.slug);
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.slug !== this.props.slug) {
      //Perform some operation
      this.setState({ slug: nextProps.slug });
    }
    this.setState({ chosenProvince: '' });
  }
  handleChange = (event) => {
    this.setState({ chosenProvince: event.target.value });
  };

  render() {
    // console.log(this.state.slug);

    var { series, classes } = this.props;
    var { openSelection, chosenProvince } = this.state;
    var endSeries = [...series];

    // groupedSeries used for keys ( as provinces ) , for selection to specific
    var groupedSeries = {};

    // IF HAVE PROVINCE => group province ==> show selection ==> modify End series
    if (series[0]) {
      // console.log('province', series[0].province !== ''); // co province
      if (series[0].province !== '') {
        groupedSeries = groupBy(series, 'province');
        openSelection = true;
      } else {
        groupedSeries = [...series];
      }
      delete groupedSeries[''];
      if (chosenProvince !== '') {
        endSeries = groupedSeries[chosenProvince];
      }
    }

    // Construct EndData based on  End Series
    const chartDataDefault = [{ value: 0, time: 0 }];

    const chartSeriesConfirmed = endSeries.map((s) => {
      return {
        value: s.confirmed,
        time: moment(s.date).format('x'),
      };
    });
    const chartSeriesDeath = endSeries.map((s) => {
      return {
        value: s.deaths,
        time: moment(s.date).format('x'),
      };
    });
    const chartSeriesRecover = endSeries.map((s) => {
      return {
        value: s.recovered,
        time: moment(s.date).format('x'),
      };
    });

    var endData = {
      chartSeriesConfirmed: chartSeriesConfirmed
        ? chartSeriesConfirmed
        : chartDataDefault,
      chartSeriesDeath: chartSeriesDeath ? chartSeriesDeath : chartDataDefault,
      chartSeriesRecover: chartSeriesRecover
        ? chartSeriesRecover
        : chartDataDefault,
    };

    return (
      <div className={classes.describeLineChartContainer}>
        {openSelection ? (
          <MySelectOption
            chosenProvince={chosenProvince}
            handleChange={this.handleChange}
            groupedSeries={groupedSeries}
          />
        ) : (
          ''
        )}
        {/* openSelection == fasle or chosenProvince = true => hien */}
        {chosenProvince || openSelection === false ? (
          <MyLineChart chartData={endData} />
        ) : (
          <div className={classes.clickHolder}>
            <p>Select 1 province to show TimeSeries</p>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    series: state.series.series,
  };
};

//send actions by dispatch
const mapDispatchToProps = (dispatch) => {
  return {
    seriesActionsCreator: bindActionCreators(seriesActions, dispatch),
  };
};
export default withStyles(ContentStyle)(
  connect(mapStateToProps, mapDispatchToProps)(DescribeLineChart)
);
