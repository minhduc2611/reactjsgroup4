import { withStyles } from '@material-ui/core';
import React, { Component } from 'react';
import ContentStyle from '../../../style';
import DescribeContent from './DescribeContent';
import DescribeLineChart from './DescribeLineChart';
class ContentDescribe extends Component {
  render() {
    var { chosenChart, classes } = this.props;

    var chosen = chosenChart ? chosenChart : {};

    var chosenSlug = chosenChart.activePayload
      ? chosenChart.activePayload[0].payload.slug
      : 0;

    return (
      <div>
        {Object.keys(chosenChart).length !== 0 ? (
          <div>
            <DescribeContent chosen={chosen} />
            <DescribeLineChart slug={chosenSlug} />
          </div>
        ) : (
          <div className={classes.clickHolder}>
            <p>Click on 1 country to show Details</p>
          </div>
        )}
      </div>
    );
  }
}

export default withStyles(ContentStyle)(ContentDescribe);
