import React from 'react';
import {
  Bar,
  BarChart,
  CartesianGrid,
  Legend,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts';
export default function MyStackChart(props) {
  var { data, onHandleChoseChart } = props;
  return (
    <ResponsiveContainer>
      <BarChart
        //   style={{ width: '300px', height: '100%' }}
        width={1000}
        height={30}
        color={'white'}
        data={data}
        margin={{
          top: 20,
          right: 30,
          left: 20,
          bottom: 5,
        }}
        onClick={(chosen) => onHandleChoseChart(chosen)}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis
          tick={{ fill: 'white' }}
          // angle={-45}
          stroke="white"
          dataKey="name"
        />
        <YAxis
          tick={{ fill: 'white' }}
          stroke="white"
          tickFormatter={(num) =>
            num.toLocaleString(navigator.language, {
              minimumFractionDigits: 0,
            })
          }
        />
        <Tooltip />
        <Legend />
        <Bar dataKey="TotalConfirmed" stackId="a" fill="#e66837" />
        <Bar dataKey="TotalRecovered" stackId="a" fill="#82ca9d" />
        <Bar dataKey="TotalDeaths" stackId="a" fill="#d6d6d6" />
      </BarChart>
    </ResponsiveContainer>
  );
}
