// presentational component

import { Button, withStyles } from '@material-ui/core';
import React from 'react';
import ContentStyle from './../style';
import ContentGoogleMap from './ContentGoogleMap';
import ContentStackChart from './ContentStackChart';
function ContentMain(props) {
  const [useChart, setUseChart] = React.useState(false);
  var { classes } = props;

  return (
    <div className={classes.container}>
      <div className={classes.btnDiv}>
        <Button
          className={classes.btn}
          variant="contained"
          color="primary"
          onClick={() => setUseChart(!useChart)}
        >
          {useChart ? 'Use Chart' : 'Use Map'}
        </Button>
      </div>
      <div
        className={classes.contentWrapper}
        style={{ display: useChart ? 'none' : 'block' }}
      >
        <ContentStackChart />
      </div>

      <div
        className={classes.contentWrapper}
        style={{ display: useChart ? 'block' : 'none' }}
      >
        <ContentGoogleMap />
      </div>
    </div>
  );
}

export default withStyles(ContentStyle)(ContentMain);
