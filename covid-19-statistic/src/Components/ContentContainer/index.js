//smart component

import { withStyles } from '@material-ui/core';
import React, { Component } from 'react';
import ContentMain from './ContentMain';
import ContentStyle from './style';
class ContentContainer extends Component {
  render() {
    var { classes } = this.props;
    return (
      <div className={classes.container}>
        <ContentMain />
      </div>
    );
  }
}
export default withStyles(ContentStyle)(ContentContainer);
