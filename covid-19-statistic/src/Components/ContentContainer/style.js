// component style here. dont forget to use theme. demo is at the App folder: style.js

const ContentStyle = (theme) => {
  var mode = theme.dark;
  return {
    text: {
      color: mode.text_primary_color.color,
    },
    bgBlack: {
      backgroundColor: mode.dark_primary_color.background,
    },
    btn: {
      fontSize: '12px',
      zIndex: 100,
    },
    btnMaps: {
      fontSize: '12px',

      position: 'relative',
      top: '10px',

      display: 'block',
      zIndex: 100,
    },
    btnDiv: {
      height: '5vh',
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(2),
      paddingTop: theme.spacing(2),
    },
    contentWrapper: {
      height: '83vh',
      // height: '100%',
    },
    container: {
      height: '88vh',
      // minHeight: '700px',
      display: 'block',
      backgroundColor: mode.dark_primary_color.background,
      // border: theme.,
      color: mode.text_primary_color.color,
      borderRadius: '5px',
    },
    containerMap: {
      position: 'relative',
      top: 12,
      height: '80vh',
      margin: 2,
      // minHeight: '700px',
      display: 'block',
      backgroundColor: mode.dark_primary_color.background,
      // border: theme.,
      color: mode.text_primary_color.color,
      borderRadius: '5px',
    },
    theMap: {
      width: '100%',
      height: '95%',
      margin: '0px',
      position: 'relative',
    },
    describeLineChartContainer: {
      width: '98%',
      height: '30%',
      position: 'absolute',
      bottom: '14%',
      alignContent: 'center',
    },
    searchContainer: {
      display: 'block',

      height: '6%',
      [theme.breakpoints.down('sm')]: {
        '& *': {
          width: '19ch',
          top: -10,
          float: 'right',
          right: 0,
        },
        width: '100%',
      },
      [theme.breakpoints.up('md')]: {
        width: '320px',

        margin: 'auto',
      },
      [theme.breakpoints.up('lg')]: {
        width: '320px',

        margin: 'auto',
      },
    },
    formControlContainerLineChart: {
      display: 'block',
      marginTop: '-67px',
      fontSize: '30px',
      [theme.breakpoints.down('sm')]: {
        width: '120px',
        margin: 'auto',
        height: '48px',
        marginTop: '-67px',
        transform: 'translate(-100px,0px)',
      },
      [theme.breakpoints.up('md')]: {
        margin: 'auto',
        width: '320px',
        height: '48px',
        marginTop: '-67px',
      },
      [theme.breakpoints.up('lg')]: {
        display: 'block',
        width: '320px',
        height: '48px',
        marginTop: '-67px',
        margin: 'auto',
      },
    },
    chartContainer: {
      height: '40%',
    },
    formControl: {
      '& label': {
        color: 'white',
      },
      '& .MuiSelect-select.MuiSelect-select': {
        // paddingRight: '24px',
        fontSize: 25,
        color: 'white',
      },
      '& label.Mui-focused': {
        color: 'white',
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: 'white',
      },
      '& .MuiOutlinedInput-root': {
        '& fieldset': {
          borderColor: 'white',
        },
        '&:hover fieldset': {
          borderColor: 'white',
        },
        '&.Mui-focused fieldset': {
          borderColor: 'white',
        },
      },
      // margin: 'auto',
      // display: 'flex',
      // alignItems: 'center',
      // background: mode.text_primary_color.color,
      borderRadius: '2px',
      width: 320,
      color: 'white !important',
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
    customTooltip: {
      background: 'white',
    },
    clickHolder: {
      marginTop: '30px',
      display: 'block',
      textAlign: 'center',
      width: '100%',
    },
    Input: {
      '& *': {
        [theme.breakpoints.down('sm')]: {
          width: '25ch',
          color: 'white',
          // fontSize: '12px',
        },
        [theme.breakpoints.up('md')]: {
          width: 320,
          color: 'white',
          fontSize: '15px',
        },
        [theme.breakpoints.up('lg')]: {
          width: 320,
          color: 'white',
          fontSize: '15px',
        },
      },
      top: '-10px',
      borderRadius: '2px',
      '& label.Mui-focused': {
        color: 'white',
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: 'white',
      },
      '& .MuiOutlinedInput-root': {
        '& fieldset': {
          borderColor: 'white',
        },
        '&:hover fieldset': {
          borderColor: 'white',
        },
        '&.Mui-focused fieldset': {
          borderColor: 'white',
        },
      },
    },
  };
};
export default ContentStyle;
