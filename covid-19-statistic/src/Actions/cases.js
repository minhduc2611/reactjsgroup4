import * as casesApis from './../APIs/cases';
import * as casesConstants from './../commons/constants/cases';
export const getCasesByCountry = () => {
  return {
    type: casesConstants.FETCH_CASES_BY_COUNTRY,
  };
};
export const getCasesByCountrySuccess = (data) => {
  return {
    type: casesConstants.FETCH_CASES_BY_COUNTRY_SUCCESS,
    payload: { data },
  };
};
export const getCasesByCountryFail = (error) => {
  return {
    type: casesConstants.FETCH_CASES_BY_COUNTRY_FAIL,
    payload: { error },
  };
};

/**
 * B1 : getCasesByCountryRequest
 * B2 : reset state => []
 * B3 : getCasesByCountrySuccess
 */
export const getCasesByCountryRequest = () => {
  let country = 'us';
  return (dispatch) => {
    dispatch(getCasesByCountry());
    casesApis
      .fetchCasesByCountry(country)
      .then((res) => {
        // console.log('cases:', res.data);
        const { data } = res;
        dispatch(getCasesByCountrySuccess(data));
      })
      .catch((error) => {
        console.log('error', error);
        dispatch(getCasesByCountryFail(error));
      });
  };
};

export const getAllCasesRequest = (countries, dispatch) => {
  dispatch(getCasesByCountry());
  countries.forEach((ct) => {
    casesApis
      .fetchCasesByCountry(ct)
      .then((res) => {
        // console.log('cases:', res);
        const { data } = res;
        dispatch(getCasesByCountrySuccess(data));
      })
      .catch((error) => {
        console.log('error', error);
        dispatch(getCasesByCountryFail(error));
      });
  });
};
