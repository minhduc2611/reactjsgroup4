import * as countriesApis from './../APIs/countries';
import * as countriesConstants from './../commons/constants/countries';
import { getAllCasesRequest } from './cases';
export const fetchListCountries = () => {
  return {
    type: countriesConstants.FETCH_COUNTRIES,
  };
};
export const fetchListCountriesSuccess = (data) => {
  return {
    type: countriesConstants.FETCH_COUNTRIES_SUCCESS,
    payload: { data },
  };
};
export const fetchListCountriesFail = (error) => {
  return {
    type: countriesConstants.FETCH_COUNTRIES_FAIL,
    payload: { error },
  };
};

/**
 * B1 : getCountriesRequest
 * B2 : reset state => []
 * B3 : fetchListCountriesSuccess
 */
export const getCountriesRequest = () => {
  return (dispatch) => {
    dispatch(fetchListCountries());
    countriesApis
      .fetchCountries()
      .then((res) => {
        // console.log('data:', res.data);
        const { data } = res;
        dispatch(fetchListCountriesSuccess(data));
      })
      .catch((error) => {
        console.log('error', error);
        dispatch(fetchListCountriesFail(error));
      });
  };
};

export const getCountriesWithCasesRequest = () => {
  return (dispatch) => {
    dispatch(fetchListCountries());
    countriesApis
      .fetchCountries()
      .then((response) => {
        // console.log('data:', response.data);
        const { data } = response;
        let countries = data.map((ct) => {
          return ct.Slug;
        });
        // console.log('countries:', countries);
        getAllCasesRequest(countries, dispatch);
        dispatch(fetchListCountriesSuccess(data));
      })
      .catch((error) => {
        console.log('error', error);
        dispatch(fetchListCountriesFail(error));
      });
  };
};
