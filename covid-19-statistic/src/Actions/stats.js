import * as statsApis from './../APIs/sumaries';
import * as statsConstants from './../commons/constants/sumaries';

export const fetchObjectStats = () => {
  return {
    type: statsConstants.FETCH_STATS,
  };
};
export const fetchObjectStatsSuccess = (data) => {
  return {
    type: statsConstants.FETCH_STATS_SUCCESS,
    payload: { data },
  };
};
export const fetchObjectStatsFail = (error) => {
  return {
    type: statsConstants.FETCH_STATS_FAIL,
    payload: { error },
  };
};

export const getStatsRequest = (dispatch) => {
  dispatch(fetchObjectStats());
  statsApis
    .fetchStats()
    .then((res) => {
      // console.log('data:', res.data);
      const { data } = res;
      dispatch(fetchObjectStatsSuccess(data));
    })
    .catch((error) => {
      console.log('error', error);
      dispatch(fetchObjectStatsFail(error));
    });
};
