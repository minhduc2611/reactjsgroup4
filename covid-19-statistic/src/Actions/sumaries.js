import * as sumaryApis from './../APIs/sumaries';
import * as sumariesConstants from './../commons/constants/sumaries';
import * as statsAction from './stats';
export const fetchObjectSumaries = () => {
  return {
    type: sumariesConstants.FETCH_SUMARIES,
  };
};
export const fetchObjectSumariesSuccess = (data) => {
  return {
    type: sumariesConstants.FETCH_SUMARIES_SUCCESS,
    payload: { data },
  };
};
export const fetchObjectSumariesFail = (error) => {
  return {
    type: sumariesConstants.FETCH_SUMARIES_FAIL,
    payload: { error },
  };
};

/**
 * B1 : getSumariesRequest
 * B2 : reset state => []
 * B3 : fetchObjectSumariesSuccess
 */
export const getSumariesRequest = () => {
  return (dispatch) => {
    dispatch(fetchObjectSumaries());
    sumaryApis
      .fetchSumary()
      .then((res) => {
        // console.log('data:', res.data);
        const { data } = res;
        statsAction.getStatsRequest(dispatch);
        dispatch(fetchObjectSumariesSuccess(data));
      })
      .catch((error) => {
        console.log('error', error);
        dispatch(fetchObjectSumariesFail(error));
      });
  };
};
