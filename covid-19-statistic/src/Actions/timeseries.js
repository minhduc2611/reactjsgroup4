import * as timeseriesApis from './../APIs/timeseries';
import * as seriesConstants from './../commons/constants/timeseries';
export const getSeriesByCountry = () => {
  return {
    type: seriesConstants.FETCH_SERIES_BY_COUNTRY,
  };
};
export const getSeriesByCountrySuccess = (data) => {
  return {
    type: seriesConstants.FETCH_SERIES_BY_COUNTRY_SUCCESS,
    payload: { data },
  };
};
export const getSeriesByCountryFail = (error) => {
  return {
    type: seriesConstants.FETCH_SERIES_BY_COUNTRY_FAIL,
    payload: { error },
  };
};

/**
 * B1 : getCasesByCountryRequest
 * B2 : reset state => []
 * B3 : getCasesByCountrySuccess
 */
export const getSeriesByCountryRequest = (country) => {
  // country = 'india';
  return (dispatch) => {
    dispatch(getSeriesByCountry());
    timeseriesApis
      .fetchSeriesByCountry(country)
      .then((res) => {
        // console.log('cases:', res.data);
        const { data } = res;
        dispatch(getSeriesByCountrySuccess(data));
      })
      .catch((error) => {
        console.log('error', error);
        dispatch(getSeriesByCountryFail(error));
      });
  };
};

// export const getAllCasesRequest = (countries, dispatch) => {
//   console.log(countries);
//   dispatch(getCasesByCountry());
//   countries.forEach((ct) => {
//     // console.log(ct);

//     casesApis
//       .fetchCasesByCountry(ct)
//       .then((res) => {
//         // console.log('cases:', res);
//         const { data } = res;
//         dispatch(getCasesByCountrySuccess(data));
//       })
//       .catch((error) => {
//         console.log('error', error);
//         dispatch(getCasesByCountryFail(error));
//       });
//   });
// };
